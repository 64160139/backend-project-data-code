import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Store } from 'src/store/entities/store.entity';
import { Customer } from 'src/customers/entities/customer.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import { OrderItem } from './order-item';

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: 0 })
  amount: number;

  @Column({ type: 'float', default: 0 })
  total: number;

  // @Column({ type: 'float' })
  // received: number;

  // @Column({ type: 'float', default: 0 })
  // change: number;

  @Column({
    type: 'varchar',
    length: 128,
    charset: 'utf8mb4', // Specify UTF-8mb4 character set
    collation: 'utf8mb4_unicode_ci', // Specify collation for Thai language
  })
  payment: string;

  // @Column({ default: null })
  // tel: string;

  // @Column({ default: 0 })
  // discount: number;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  @ManyToOne(() => Employee, (employee) => employee.order)
  employee: Employee;

  @ManyToOne(() => Customer, (customer) => customer.orders)
  customer: Customer;

  @ManyToOne(() => Store, (store) => store.orders)
  store: Store;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.order)
  orderItems: OrderItem[];
}
