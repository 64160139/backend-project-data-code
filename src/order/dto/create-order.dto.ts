import { IsNotEmpty } from 'class-validator';

class CreatedOrderItemDto {
  @IsNotEmpty()
  productId: number;

  @IsNotEmpty()
  amount: number;
}

export class CreateOrderDto {
  @IsNotEmpty()
  orderItems: CreatedOrderItemDto[];

  @IsNotEmpty()
  payment: string;

  // @IsNotEmpty()
  // received: number;

  // @IsNotEmpty()
  // tel: string;

  @IsNotEmpty()
  storeId: number;

  @IsNotEmpty()
  employeeId: number;

  @IsNotEmpty()
  customerId: number;
}
