import { isNotEmpty } from 'class-validator';
import { CheckInOut } from 'src/check_in_out/entities/check_in_out.entity';
import { Order } from 'src/order/entities/order.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToOne,
  OneToMany,
} from 'typeorm';

@Entity()
export class Employee {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  name: string;

  @Column({
    length: '32',
  })
  phone: string;

  @Column()
  email: string;

  @Column({
    type: 'varchar',
    length: 32,
    charset: 'utf8mb4', // Specify UTF-8mb4 character set
    collation: 'utf8mb4_unicode_ci', // Specify collation for Thai language
  })
  position: string;

  @Column()
  hourly_wage: number;

  @Column()
  address: string;

  @OneToOne(() => User, (user) => user.employee)
  user: User;

  @OneToMany(() => Order, (order) => order.employee)
  order: Order[];

  @OneToMany(() => CheckInOut, (checkinout) => checkinout.employee)
  checkinout: CheckInOut[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
