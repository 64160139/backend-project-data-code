import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import * as bcrypt from 'bcrypt';
import { Employee } from './entities/employee.entity';
import { Repository } from 'typeorm';
import { User } from 'src/users/entities/user.entity';
@Injectable()
export class EmployeesService {
  constructor(
    @InjectRepository(Employee)
    private employeesRepository: Repository<Employee>,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async create(createEmployeeDto: CreateEmployeeDto) {
    const NewEmp = new Employee();
    NewEmp.name = createEmployeeDto.name;
    NewEmp.address = createEmployeeDto.address;
    NewEmp.phone = createEmployeeDto.phone;
    NewEmp.email = createEmployeeDto.email;
    NewEmp.position = createEmployeeDto.position;
    NewEmp.hourly_wage = createEmployeeDto.hourly_wage;
    const user = await this.usersRepository.findOne({
      where: { email: createEmployeeDto.email },
    });
    if (!user) {
      throw new NotFoundException();
    }
    NewEmp.user = user;
    return this.employeesRepository.save(NewEmp);
  }

  findAll() {
    return this.employeesRepository.find({});
  }

  async findOne(id: number) {
    const employee = await this.employeesRepository.findOne({
      where: { id: id },
    });
    if (!employee) {
      throw new NotFoundException();
    }
    return employee;
  }

  async update(id: number, updateEmployeeDto: UpdateEmployeeDto) {
    const employee = await this.employeesRepository.findOneBy({ id: id });
    if (!employee) {
      throw new NotFoundException();
    }
    const updatedEmployee = { ...employee, ...updateEmployeeDto };
    return this.employeesRepository.save(updatedEmployee);
  }

  async remove(id: number) {
    const employee = await this.employeesRepository.findOneBy({ id: id });
    if (!employee) {
      throw new NotFoundException();
    }
    return this.employeesRepository.softRemove(employee);
  }
}
