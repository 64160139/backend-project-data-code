import { Category } from 'src/category/entities/category.entity';
import { OrderItem } from 'src/order/entities/order-item';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
    charset: 'utf8mb4', // Specify UTF-8mb4 character set
    collation: 'utf8mb4_unicode_ci', // Specify collation for Thai language
  })
  name: string;

  @Column()
  type: string;

  @Column({
    type: 'varchar',
    length: 128,
    charset: 'utf8mb4', // Specify UTF-8mb4 character set
    collation: 'utf8mb4_unicode_ci', // Specify collation for Thai language
  })
  img: string;

  @Column({
    type: 'float',
  })
  price: number;

  @ManyToOne(() => Category, (category) => category.products)
  @JoinColumn({ name: 'category_id' })
  category: Category;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.product)
  orderItems: OrderItem[];
}
