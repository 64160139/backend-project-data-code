export class CreateCustomerDto {
  codeCus: string;

  name: string;

  phone: string;

  points: number;
}
