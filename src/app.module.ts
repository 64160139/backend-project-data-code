import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CustomersModule } from './customers/customers.module';
import { Customer } from './customers/entities/customer.entity';
import { Product } from './products/entities/product.entity';
import { ProductsModule } from './products/products.module';
import { UsersModule } from './users/users.module';
import { User } from './users/entities/user.entity';
import { AuthModule } from './auth/auth.module';
import { EmployeesModule } from './employees/employees.module';
import { Employee } from './employees/entities/employee.entity';
import { StoreModule } from './store/store.module';
import { Store } from './store/entities/store.entity';
import { CategoryModule } from './category/category.module';
import { Category } from './category/entities/category.entity';
import { CheckInOutModule } from './check_in_out/check_in_out.module';
import { CheckInOut } from './check_in_out/entities/check_in_out.entity';
import { SummarySalaryModule } from './summary_salary/summary_salary.module';
import { SummarySalary } from './summary_salary/entities/summary_salary.entity';
import { ReportsModule } from './reports/reports.module';
import { MaterialsModule } from './materials/materials.module';
import { Material } from './materials/entities/material.entity';
import { OrderModule } from './order/order.module';
import { Order } from './order/entities/order.entity';
import { OrderItem } from './order/entities/order-item';

@Module({
  imports: [
    TypeOrmModule.forRoot(
      //   {
      //   type: 'sqlite',
      //   database: 'db.sqlite',
      //   synchronize: true,
      //   migrations: [],
      //   entities: [Customer, Product, Order, OrderItem, User],
      // }
      {
        type: 'mysql',
        host: 'angsila.informatics.buu.ac.th',
        port: 3306,
        username: 'guest08',
        password: 'Th9Mucxf',
        database: 'guest08',
        entities: [
          OrderItem,
          Order,
          Customer,
          Product,
          User,
          Employee,
          Store,
          Category,
          CheckInOut,
          SummarySalary,
          Material,
        ],
        synchronize: true,
      },
    ),
    OrderModule,
    CustomersModule,
    ProductsModule,
    UsersModule,
    AuthModule,
    EmployeesModule,
    StoreModule,
    CategoryModule,
    CheckInOutModule,
    SummarySalaryModule,
    ReportsModule,
    MaterialsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
