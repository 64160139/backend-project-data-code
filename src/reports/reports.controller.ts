import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { ReportsService } from './reports.service';
import { CreateReportDto } from './dto/create-report.dto';
import { UpdateReportDto } from './dto/update-report.dto';

@Controller('reports')
export class ReportsController {
  constructor(private readonly reportsService: ReportsService) {}

  // @Get('/customerSP')
  // getCustomerSP() {
  //   return this.reportsService.getCustomerSP();
  // }

  @Get('/customer_view')
  getCustomer_info() {
    return this.reportsService.getCustomer_info();
  }

  @Get('/countOrderItem_view')
  getCount_OrderItem_info() {
    return this.reportsService.getCount_OrderItem_info();
  }
  @Get('/OrderItem_view_desc')
  getOrderItem_info_DESC() {
    return this.reportsService.getOrderItem_info_DESC();
  }

  @Get('/OrderItem_view_asc')
  getOrderItem_info_ASC() {
    return this.reportsService.getOrderItem_info_ASC();
  }

  @Get('/Monthly')
  getMonthly() {
    return this.reportsService.getMonthly();
  }

  @Get('/Yearly')
  getYearly() {
    return this.reportsService.getYearly();
  }
  // @Post()
  // create(@Body() createReportDto: CreateReportDto) {
  //   return this.reportsService.create(createReportDto);
  // }

  // @Get()
  // findAll() {
  //   return this.reportsService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.reportsService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateReportDto: UpdateReportDto) {
  //   return this.reportsService.update(+id, updateReportDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.reportsService.remove(+id);
  // }
}
