import { Injectable } from '@nestjs/common';
import { CreateReportDto } from './dto/create-report.dto';
import { UpdateReportDto } from './dto/update-report.dto';
import { InjectDataSource } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';

@Injectable()
export class ReportsService {
  constructor(@InjectDataSource() private dataSource: DataSource) {}

  getCustomer_info() {
    return this.dataSource.query('SELECT * FROM `getCustomer_info`');
  }

  getCount_OrderItem_info() {
    throw this.dataSource.query('SELECT * FROM `getCount_OrderItem_info`');
  }
  getOrderItem_info_DESC() {
    return this.dataSource.query('SELECT * FROM `getOrderItem_info_view_DESC`');
  }
  getOrderItem_info_ASC() {
    return this.dataSource.query('SELECT * FROM `getOrderItem_info_view_ASC`');
  }

  getMonthly() {
    return this.dataSource.query('SELECT * FROM `monthly_sales_view`');
  }

  getYearly() {
    return this.dataSource.query('SELECT * FROM `yearly_sales_view`');
  }
  create(createReportDto: CreateReportDto) {
    return 'This action adds a new report';
  }

  findAll() {
    return `This action returns all reports`;
  }

  findOne(id: number) {
    return `This action returns a #${id} report`;
  }

  update(id: number, updateReportDto: UpdateReportDto) {
    return `This action updates a #${id} report`;
  }

  remove(id: number) {
    return `This action removes a #${id} report`;
  }
}
