import { IsNotEmpty } from 'class-validator';

export class CreateSummarySalaryDto {
  @IsNotEmpty()
  date: string;

  @IsNotEmpty()
  workhour: number;

  @IsNotEmpty()
  salary: number;
}
