import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateSummarySalaryDto } from './dto/create-summary_salary.dto';
import { UpdateSummarySalaryDto } from './dto/update-summary_salary.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { SummarySalary } from './entities/summary_salary.entity';
import { Repository } from 'typeorm';

@Injectable()
export class SummarySalaryService {
  constructor(
    @InjectRepository(SummarySalary)
    private summarysalarysRepository: Repository<SummarySalary>,
  ) {}

  create(createSummarySalaryDto: CreateSummarySalaryDto) {
    return this.summarysalarysRepository.save(createSummarySalaryDto);
  }

  findAll() {
    return this.summarysalarysRepository.find();
  }

  findOne(id: number) {
    return this.summarysalarysRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateSummarySalaryDto: UpdateSummarySalaryDto) {
    try {
      const updatedSummarySalary = await this.summarysalarysRepository.save({
        id,
        ...updateSummarySalaryDto,
      });
      return updatedSummarySalary;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const summarysalary = await this.summarysalarysRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedSummerySalary = await this.summarysalarysRepository.remove(
        summarysalary,
      );
      return deletedSummerySalary;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
