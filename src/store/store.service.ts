import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateStoreDto } from './dto/create-store.dto';
import { UpdateStoreDto } from './dto/update-store.dto';
import { Store } from './entities/store.entity';

@Injectable()
export class StoreService {
  constructor(
    @InjectRepository(Store)
    private storeRepository: Repository<Store>,
  ) {}

  create(createStoreDto: CreateStoreDto) {
    return this.storeRepository.save(createStoreDto);
  }

  findAll() {
    return this.storeRepository.find({});
  }

  async findOne(id: number) {
    const store = await this.storeRepository.findOne({
      where: { id: id },
    });
    if (!store) {
      throw new NotFoundException();
    }
    return store;
  }

  async update(id: number, updateStoreDto: UpdateStoreDto) {
    const store = await this.storeRepository.findOne({
      where: { id: id },
    });
    if (!store) {
      throw new NotFoundException();
    }
    const updatedStore = { ...store, ...updateStoreDto };
    return this.storeRepository.save(updatedStore);
  }

  async remove(id: number) {
    const store = await this.storeRepository.findOne({
      where: { id: id },
    });
    if (!store) {
      throw new NotFoundException();
    }
    return this.storeRepository.softRemove(store);
  }
}
