import { Order } from 'src/order/entities/order.entity';

import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';

@Entity()
export class Store {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 32 })
  name: string;

  @Column()
  address: string;

  @Column({ length: 10 })
  tel: string;

  @OneToMany(() => Order, (order) => order.store)
  orders: Order[];
}
