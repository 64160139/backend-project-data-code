import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { Category } from './entities/category.entity';

@Injectable()
export class CategoryService {
  constructor(
    @InjectRepository(Category)
    private categoriesRepository: Repository<Category>,
  ) {}

  create(createCategoryDto: CreateCategoryDto) {
    return this.categoriesRepository.save(createCategoryDto);
  }

  findAll() {
    return this.categoriesRepository.find({});
  }

  findOne(id: number) {
    return this.categoriesRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateCategoryDto: UpdateCategoryDto) {
    try {
      const updatedCategory = await this.categoriesRepository.save({
        id,
        ...updateCategoryDto,
      });
      return updatedCategory;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const category = await this.categoriesRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedCategory = await this.categoriesRepository.remove(category);
      return deletedCategory;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
