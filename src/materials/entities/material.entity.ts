import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Material {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'nvarchar', collation: 'utf8mb4_unicode_ci' })
  name: string;

  @Column({ type: 'nvarchar', collation: 'utf8mb4_unicode_ci' })
  unit: string;

  @Column()
  quantity: number;
}
