import {
  IsNotEmpty,
  Length,
  IsEmail,
  Matches,
  isNotEmpty,
} from 'class-validator';
export class CreateMaterialDto {
  @IsNotEmpty()
  id: number;

  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  unit: string;

  @IsNotEmpty()
  quantity: number;
}
