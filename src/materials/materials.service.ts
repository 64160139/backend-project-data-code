import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMaterialDto } from './dto/create-material.dto';
import { UpdateMaterialDto } from './dto/update-material.dto';
import { Material } from './entities/material.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class MaterialsService {
  constructor(
    @InjectRepository(Material)
    private materialRepository: Repository<Material>,
  ) {}
  create(createMaterialDto: CreateMaterialDto) {
    const NewMat = new Material();
    NewMat.name = createMaterialDto.name;
    NewMat.unit = createMaterialDto.unit;
    NewMat.quantity = createMaterialDto.quantity;
    return this.materialRepository.save(NewMat);
  }

  findAll() {
    return this.materialRepository.find({});
  }

  async findOne(id: number) {
    const material = await this.materialRepository.findOne({
      where: { id: id },
      relations: [],
    });
    if (!material) {
      throw new NotFoundException();
    }
    return material;
  }

  async update(id: number, updateMaterialDto: UpdateMaterialDto) {
    const material = await this.materialRepository.findOneBy({ id: id });
    if (!material) {
      throw new NotFoundException();
    }
    const updatematerial = { ...material, ...updateMaterialDto };
    return this.materialRepository.save(updatematerial);
  }

  async remove(id: number) {
    const material = await this.materialRepository.findOneBy({ id: id });
    if (!material) {
      throw new NotFoundException();
    }
    return this.materialRepository.remove(material);
  }
}
