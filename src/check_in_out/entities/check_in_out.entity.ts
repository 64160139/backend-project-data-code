import { Transform } from 'class-transformer';
import { Employee } from 'src/employees/entities/employee.entity';

import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
function formatDate(date: Date): string {
  const day = date.getDate().toString().padStart(2, '0');
  const month = (date.getMonth() + 1).toString().padStart(2, '0');
  const year = date.getFullYear().toString();
  return `${day}/${month}/${year}`;
}

@Entity()
export class CheckInOut {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: new Date().toLocaleDateString() })
  date: string;

  @Column({ default: new Date().toLocaleDateString() })
  time_in: string;

  @Column({ nullable: true })
  time_out: string;

  @Column()
  total_hour: string;

  @ManyToOne(() => Employee, (employee) => employee.checkinout)
  employee: Employee;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
