import { Module } from '@nestjs/common';
import { CheckInOutService } from './check_in_out.service';
import { CheckInOutController } from './check_in_out.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CheckInOut } from './entities/check_in_out.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import { User } from 'src/users/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CheckInOut, Employee, User])],
  controllers: [CheckInOutController],
  providers: [CheckInOutService],
  exports: [CheckInOutService],
})
export class CheckInOutModule {}
