import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Employee } from 'src/employees/entities/employee.entity';
import { Not, IsNull, Repository } from 'typeorm';
import { CreateCheckInOutDto } from './dto/create-check_in_out.dto';
import { UpdateCheckInOutDto } from './dto/update-check_in_out.dto';
import { CheckInOut } from './entities/check_in_out.entity';
import { IsNotEmpty } from 'class-validator';
import { format } from 'date-fns-tz';
import { User } from 'src/users/entities/user.entity';
import moment from 'moment-timezone';

@Injectable()
export class CheckInOutService {
  constructor(
    @InjectRepository(Employee)
    private employeesRepository: Repository<Employee>,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @InjectRepository(CheckInOut)
    private checkinoutsRepository: Repository<CheckInOut>,
  ) {}

  async create(createCheckInOutDto: CreateCheckInOutDto) {
    const employee = await this.employeesRepository.findOne({
      where: { email: createCheckInOutDto.email },
    });
    const user = await this.usersRepository.findOne({
      where: { email: createCheckInOutDto.email },
    });
    if (!employee && !user) {
      throw new NotFoundException();
    }

    // check if employee has checked-out before checking-in again
    const lastCheckOut = await this.checkinoutsRepository.findOne({
      where: {
        employee: employee,
        time_out: Not(IsNull()),
      },
      order: {
        id: 'DESC',
      },
    });
    if (lastCheckOut && lastCheckOut.time_out < lastCheckOut.time_in) {
      throw new BadRequestException('Employee has not checked-out yet.');
    }

    const lastCheckIn = await this.checkinoutsRepository.findOne({
      where: {
        employee: employee,
        time_out: IsNull(),
      },
    });
    if (lastCheckIn) {
      throw new BadRequestException('Employee has not checked-out yet.');
    }

    const checkIn = new CheckInOut();
    checkIn.employee = employee;
    checkIn.total_hour = '';
    checkIn.date = new Date().toLocaleDateString();
    checkIn.time_in = new Date().toLocaleTimeString();
    return this.checkinoutsRepository.save(checkIn);
  }

  findAll() {
    return this.checkinoutsRepository.find();
  }

  findOne(id: number) {
    return `This action returns a #${id} checkInOut111`;
  }

  async findOne1(email: string) {
    const check = await this.checkinoutsRepository.find({
      where: {
        employee: { email },
      },
      relations: ['employee'],
    });
    return check;
  }

  async update(updateCheckInOutDto: UpdateCheckInOutDto) {
    const check = await this.checkinoutsRepository.findOne({
      where: {
        employee: { email: updateCheckInOutDto.email },
        time_out: IsNull(),
      },
      relations: ['employee'],
    });
    if (!check) {
      throw new NotFoundException();
    }
    const timeIn = check.createdAt.getTime();
    const timeOut = new Date().getTime();
    const time = Math.abs(timeOut - timeIn);
    const hours = Math.floor(time / (1000 * 60 * 60));
    const minutes = Math.floor((time / (1000 * 60)) % 60);
    check.total_hour = `${hours}:${minutes.toString().padStart(2, '0')}`;
    check.time_out = new Date().toLocaleTimeString();
    return this.checkinoutsRepository.save(check);
  }

  remove(id: number) {
    return `This action removes a #${id} checkInOut`;
  }
}
