import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { CheckInOutService } from './check_in_out.service';
import { CreateCheckInOutDto } from './dto/create-check_in_out.dto';
import { UpdateCheckInOutDto } from './dto/update-check_in_out.dto';

@Controller('check-in-out')
export class CheckInOutController {
  constructor(private readonly checkInOutService: CheckInOutService) {}

  @Post()
  create(@Body() createCheckInOutDto: CreateCheckInOutDto) {
    return this.checkInOutService.create(createCheckInOutDto);
  }

  @Get()
  findAll() {
    return this.checkInOutService.findAll();
  }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.checkInOutService.findOne(+id);
  // }

  @Get('email/:email')
  check(@Param('email') email: string) {
    return this.checkInOutService.findOne1(email);
  }

  @Patch()
  update(@Body() updateCheckInOutDto: UpdateCheckInOutDto) {
    return this.checkInOutService.update(updateCheckInOutDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.checkInOutService.remove(+id);
  }
}
